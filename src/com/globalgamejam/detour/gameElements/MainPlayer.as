package com.globalgamejam.detour.gameElements
{
	import flash.geom.Point;
	
	import ui.Obstacle;
	import ui.Player;

	public class MainPlayer extends Player
	{
		public static var MOVE_UP:String = "up";
		public static var MOVE_DOWN:String = "down";
		public static var MOVE_LEFT:String = "left";
		public static var MOVE_RIGHT:String = "right";
		
		public var speed:Number = 6;
		
		private var obstacle:Obstacle;
		
		public function MainPlayer(obj:Obstacle){
			obstacle = obj;
		}
		
		public function move(type:String):void{
			var pos:Point = new Point(this.x,this.y);
			if(type == MOVE_UP){
				pos.y -= speed;
			}else if(type == MOVE_DOWN){
				pos.y += speed;
			}else if(type == MOVE_LEFT){
				pos.x -= speed;
			}else if(type == MOVE_RIGHT){
				pos.x += speed;
			}
			if(!detectionObstacle(pos)){
				this.x = pos.x;
				this.y = pos.y;
			}
		}
		
		private function detectionObstacle(pos:Point):Boolean{
			return (obstacle.hitTestPoint(pos.x,pos.y+25,true) || obstacle.hitTestPoint(pos.x +25,pos.y,true) 
				|| obstacle.hitTestPoint(pos.x,pos.y-25,true) || obstacle.hitTestPoint(pos.x -25,pos.y,true));
		}
	}
}