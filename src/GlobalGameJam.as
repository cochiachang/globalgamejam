package
{
	import com.globalgamejam.detour.gameElements.GameControlPanel;
	import com.globalgamejam.detour.gameElements.MainPlayer;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	
	import ui.Obstacle;
	
	[SWF(frameRate="24",width="960", height="640", backgroundColor="#FFFFFF")]
	public class GlobalGameJam extends Sprite
	{
		private var controlPanel:GameControlPanel;
		private var player:MainPlayer;
		
		private var direction:String;
		
		public function GlobalGameJam()
		{
			super();
			
			this.stage.scaleMode = StageScaleMode.EXACT_FIT;
			this.stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(KeyboardEvent.KEY_DOWN,movePeopleHandler);
			
			this.addEventListener(Event.ADDED, onAddedToDisplayList);
		}
		
		private function onAddedToDisplayList(e:Event):void {
			this.removeEventListener(Event.ADDED, onAddedToDisplayList);
			this.addEventListener(Event.ENTER_FRAME, movePeopleHandler);
			//增加障礙物
			var obj:Obstacle = new Obstacle();
			this.addChild(obj);
			//增加按鈕
			controlPanel = new GameControlPanel();
			controlPanel.upBtn.addEventListener(MouseEvent.MOUSE_DOWN, TouchMoveStartHandler);
			controlPanel.downBtn.addEventListener(MouseEvent.MOUSE_DOWN, TouchMoveStartHandler);
			controlPanel.leftBtn.addEventListener(MouseEvent.MOUSE_DOWN, TouchMoveStartHandler);
			controlPanel.rightBtn.addEventListener(MouseEvent.MOUSE_DOWN, TouchMoveStartHandler);
			
			controlPanel.upBtn.addEventListener(MouseEvent.MOUSE_UP, TouchMoveEndHandler);
			controlPanel.downBtn.addEventListener(MouseEvent.MOUSE_UP, TouchMoveEndHandler);
			controlPanel.leftBtn.addEventListener(MouseEvent.MOUSE_UP, TouchMoveEndHandler);
			controlPanel.rightBtn.addEventListener(MouseEvent.MOUSE_UP, TouchMoveEndHandler);

			controlPanel.x = 840;
			controlPanel.y = 10;
			this.addChild(controlPanel);
			//增加主角
			player = new MainPlayer(obj);
			player.x = 100;
			player.y = 50;
			this.addChild(player);
			//增加光源
		}
		
		private function TouchMoveStartHandler(e:Event):void{
			if(e.target == controlPanel.upBtn){
				direction = MainPlayer.MOVE_UP;
			}else if(e.target == controlPanel.downBtn){
				direction = MainPlayer.MOVE_DOWN;
			}else if(e.target == controlPanel.leftBtn){
				direction = MainPlayer.MOVE_LEFT;
			}else if(e.target == controlPanel.rightBtn){
				direction = MainPlayer.MOVE_RIGHT;
			}
		}
		
		private function TouchMoveEndHandler(e:Event):void{
			direction = '';
		}
		
		private function movePeopleHandler(e:Event):void{
			
			trace(".............");
			if(direction != ''){
				player.move(direction);
				trace(".............");
			}
		}
	}
}